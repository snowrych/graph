#-------------------------------------------------
#
# Project created by QtCreator 2020-09-23T10:15:55
#
#-------------------------------------------------

QT += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Graph_widget
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        src/main.cpp \
    src/Models/Wire.cpp \
    src/Models/Port.cpp \
    src/Models/Device.cpp \
    src/ViewModels/DeviceItem.cpp \
    src/ViewModels/WireItem.cpp \
    src/ViewModels/PortItem.cpp \
    src/GuiManager/MainWindow.cpp \
    src/GuiManager/GraphScene.cpp

HEADERS += \
    src/Utility/Macros.h \
    src/Models/Wire.h \
    src/Models/Port.h \
    src/Models/Device.h \
    src/ViewModels/DeviceItem.h \
    src/ViewModels/WireItem.h \
    src/ViewModels/PortItem.h \
    src/GuiManager/MainWindow.h \
    src/GuiManager/GraphScene.h

RESOURCES += \
    sources.qrc
