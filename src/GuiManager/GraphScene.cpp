#include "src/GuiManager/GraphScene.h"

GraphScene::GraphScene(QObject *parent) : QGraphicsScene(parent)  {
    setItemIndexMethod(ItemIndexMethod::NoIndex);
    setMode(MoveItem);
    setDirected(true); // for test
    device_id = 0;//TODO чтение из модели
    connection_id = 0; //TODO чтение из модели
    port_id = 0;//TODO чтение из модели

    startNode = nullptr;
    endNode = nullptr;
}
//
void GraphScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(m_Mode == InsertLine) {
        QList<QGraphicsItem*> clickedItems = items(mouseEvent->scenePos());
        if (clickedItems.size()) {
            QGraphicsItem* item = clickedItems[0];
            if(startNode && item->type() == PortItem::Type) {
                endNode = dynamic_cast<PortItem*>(item);
                if(endNode != startNode) {
                    // do draw
                    qDebug() << "endNode" << endNode;
                    Wire* newWire = new Wire(connection_id++,startNode->portData()->id(),endNode->portData()->id(),Wire::rj45); //TODO изменить
                    WireItem* newConnection = new WireItem(startNode,endNode,newWire);
                    startNode->addConnection(newConnection);
                    endNode->addConnection(newConnection);
                    addItem(newConnection);
                    setMode(MoveItem);
                } else {
                    emit errorMessage("Вы выбрали тот же элемент");
                }
            }else if (!startNode) {
                qDebug() << " no starNode!";
                return;
            }
        }
    } else if(m_Mode == MoveItem) {
        QGraphicsScene::mousePressEvent(mouseEvent);
    }
    this->update();

}

void GraphScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(m_Mode == MoveItem) {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
    this->update();
}
void GraphScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsScene::mouseReleaseEvent(mouseEvent);
    this->update();
}

void GraphScene::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QList<QGraphicsItem*> clickedItems = items(event->scenePos());
    if (clickedItems.size()) {
        QGraphicsItem* item = clickedItems[0];
        switch (item->type()) {
        case DeviceItem::Type: {
            QMenu menu;
            menu.addAction("&Удалить");
            menu.addSeparator();
            menu.addAction("&Добавить порт");
            QAction *act = menu.exec(event->screenPos());
            if (act) {
                auto parentDevice = dynamic_cast<DeviceItem*>(item);
                if (act->text() == "&Удалить") {
                    parentDevice->clearPorts();
                    removeItem(parentDevice);
                    m_devices.removeOne(parentDevice);
                    delete parentDevice; //?
                }
                if (act->text() == "&Добавить порт") {

                    port = new Port(port_id++,parentDevice->deviceData()->id(),Port::PortType::rj45);  //TODO <--- read data from db

                    PortItem* portItem = new PortItem(port);
                    portItem->setParentItem(parentDevice);
                    parentDevice->addPort(portItem);
                    portItem->setPos(parentDevice->mapFromScene(event->scenePos()));
                    portItem->show();
                    setMode(GraphScene::Mode::MoveItem);
                }
            }
            break;
        }
        case PortItem::Type: {
            QMenu menu;
            menu.addAction("&Удалить порт");
            menu.addSeparator();
            menu.addAction("&Соеденить с (Выберите мышью 2й эл.)");
            QAction *act = menu.exec(event->screenPos());
            if (act) {
                auto portDevice = dynamic_cast<PortItem*>(item);
                if (act->text() == "&Удалить порт") {
                    DeviceItem *parentdevice = dynamic_cast<DeviceItem *>(portDevice->parentItem());
                    portDevice->clearConnections();
                    parentdevice->deletePort(portDevice);
                    removeItem(portDevice);
                    delete portDevice;
                }
                if (act->text().contains("&Соеденить с")) {
                    startNode = portDevice;
                    qDebug() << "startNode " << startNode;
                    setMode(InsertLine);
                }
            }
            break;
        }
        case WireItem::Type: {
            QMenu menu;
            menu.addAction("&Удалить связь");
            menu.addSeparator();
            menu.addAction("&Изменить тип связи");
            QAction *act = menu.exec(event->screenPos());
            if(act) {
                auto connection = dynamic_cast<WireItem*>(item);
                if (act->text() == "&Удалить связь") {
                    connection->startItem()->removeConnection(connection);
                    connection->endItem()->removeConnection(connection);
                    removeItem(connection);
                    delete connection;
                }
            }
        }
            break;
        }
    } else {
        QMenu menu;
        menu.addAction("&Добавить элемент");
        QAction *act = menu.exec(event->screenPos());
        if (act != nullptr) {
            if (act->text() == "&Добавить элемент") {
                obj = new Device(device_id++,Device::Router);  //TODO <--- read data from db
                DeviceItem* deviceItem = new DeviceItem(obj);//TODO ДОБАВЛЕНИЕ в глобальную структуру относ.к сцене (m_graph)
                m_devices.append(deviceItem);
                deviceItem->setPos(event->scenePos());
                addItem(deviceItem);
                setMode(GraphScene::Mode::MoveItem);

            }
        }
    }
}

bool GraphScene::directed() const
{
    return m_directed;
}

void GraphScene::setDirected(bool directed)
{
    m_directed = directed;
}

void GraphScene::removeDevice(DeviceItem *device)
{
    int index = m_devices.indexOf(device);

    if (index != -1)
        m_devices.removeAt(index);
}

GraphScene::Mode GraphScene::getMode()
{
    return m_Mode;
}


void GraphScene::setMode(GraphScene::Mode mode)
{
    m_Mode = mode;
}

