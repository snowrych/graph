#ifndef WIDGETSCENE_H
#define WIDGETSCENE_H

#include <QGraphicsScene>
//#include <QGraphicsItem>
#include <QMenu>
#include <QDebug>
#include <QSharedPointer>
#include <QMessageBox>
#include "src/ViewModels/DeviceItem.h"
#include "src/ViewModels/WireItem.h"
#include "src/ViewModels/PortItem.h"

class PortItem;
class DeviceItem;
class  GraphScene : public QGraphicsScene
{
    Q_OBJECT
public:
    //    explicit WidgetScene(QMenu *itemMenu, QObject *parent = nullptr);
    explicit GraphScene(QObject *parent = nullptr);
    enum Mode {InsertItem,InsertLine,MoveItem};

    Mode getMode();
//    void createEdge(bool directed);
    //    void deleteItem();
    bool directed() const;
    void setDirected(bool directed);
//    QList <DeviceItem*> getDevices() const { return m_devices; }
    void removeDevice(DeviceItem *device);

public slots:
    void setMode(Mode mode);
    //    void setItemType(const WidgetItem::WidgetItemType &ItemType);
//    void setLineType(const WidgetLine::WidgetLineType &LineType);
signals:
//    void itemInserted(DeviceItem *item);
    void itemSelected(QGraphicsItem *item);
    void errorMessage(const QString& errorMessage);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
    virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent* event) override;


private:
    bool isItemChange(int type);

    /* types */
//    WorkspaceGraph* m_graph;
    Mode m_Mode;
    Device* obj;
    Port* port;
    Wire * wire;
    int device_id;
     int connection_id;
     int port_id;

    PortItem* startNode;
    PortItem* endNode;
    QList <DeviceItem*> m_devices;
    bool m_directed;
};

#endif // WIDGETSCENE_H
