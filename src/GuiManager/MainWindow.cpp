#include "MainWindow.h"

MainWindow::MainWindow()
{

    createMenus();
    createToolBox();
    toolBox->setVisible(false);
    createToolBar();

    scene = new GraphScene(this);
    view = new QGraphicsView(this);
    view->setScene(scene);
    scene->setSceneRect(QRectF(0, 0, 2500, 2500));
    view->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse); //
    view->setCacheMode(QGraphicsView::CacheBackground); // Кэш фона

    QObject::connect(scene, &GraphScene::errorMessage, [this] (const QString& errorMessage) {
        QMessageBox::warning(this, this->windowTitle(), errorMessage);
    });

    //    connect(scene, SIGNAL(itemInserted(WidgetItem*)),this, SLOT(itemInserted(WidgetItem*)));

    QHBoxLayout *boxLayout = new QHBoxLayout;
    boxLayout->addWidget(toolBox);
    boxLayout->addWidget(view);
    //TODO boxLayout->addWidget(tabel item Property);

    QWidget *widget = new QWidget;
    widget->setLayout(boxLayout);
    setCentralWidget(widget);
    setUnifiedTitleAndToolBarOnMac(true);

}

void MainWindow::createMenus()
{
    QAction *openFile = new QAction("&Открыть файл", this);
    QAction *saveFile = new QAction("&Сохранить файл", this);
    QAction *exitAction = new QAction("&Выход", this);


    fileMenu = menuBar()->addMenu("&Файл");
    fileMenu->addAction(openFile);
    fileMenu->addAction(saveFile);
    fileMenu->addAction(exitAction);

    QObject::connect(exitAction,&QAction::triggered, [this] (bool) {
        this->close();
    });

    QAction *clearAction = new QAction("&Очистить", this);
    menuBar()->addSeparator();
    editorMenu = menuBar()->addMenu("&Редактор");
    editorMenu->addAction(clearAction);


}

void MainWindow::createToolBox()
{
    /* backgrounds buttons */
    backgroundButtonGroup = new QButtonGroup(this);
    connect(backgroundButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)),this, SLOT(backgroundsButtonClicked(QAbstractButton*)));

    QGridLayout *backgroundLayout = new QGridLayout;
    backgroundLayout->addWidget(createBackgroundWidget(tr("Белый фон"),
                                                       ":/icons/backgrounds/background1.png"), 0, 0);
    backgroundLayout->addWidget(createBackgroundWidget(tr("Ч-Б клетка"),
                                                       ":/icons/backgrounds/background2.png"), 0, 1);
    backgroundLayout->addWidget(createBackgroundWidget(tr("Серая клетка"),
                                                       ":/icons/backgrounds/background3.png"), 1, 0);
    backgroundLayout->setRowStretch(2, 10);
    backgroundLayout->setColumnStretch(2, 10);

    QWidget *backgroundWidget = new QWidget;
    backgroundWidget->setLayout(backgroundLayout);


    toolBox = new QToolBox;
    toolBox->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored));

    toolBox->setMinimumWidth(backgroundWidget->sizeHint().width());
    toolBox->addItem(backgroundWidget, tr("Фон редактора"));
}

void MainWindow::createToolBar()
{
    editToolBar = addToolBar(tr("Редактирование"));
    //    editToolBar->addAction(toFrontAction);
    QToolButton *pointerButton = new QToolButton;
    pointerButton->setCheckable(true);
    pointerButton->setChecked(true);
    pointerButton->setIcon(QIcon(":/icons/pointers/pointer.png"));

    QToolButton *linePointerButton = new QToolButton;
    linePointerButton->setCheckable(true);
    linePointerButton->setIcon(QIcon(":/icons/pointers/linepointer.png"));

    deleteAction = new QAction(QIcon(":/icons/tools/remove.png"), tr("&Delete"), this);
    deleteAction->setShortcut(tr("Delete"));
    connect(deleteAction, SIGNAL(triggered()), this, SLOT(deleteItem()));


    pointersGroup = new QButtonGroup(this);
    pointersGroup->addButton(pointerButton, int(GraphScene::MoveItem));
    pointersGroup->addButton(linePointerButton, int(GraphScene::InsertLine));

    connect(pointersGroup,SIGNAL(buttonClicked(int)),this,SLOT(pointerClicked(int)));

    editToolBar->addWidget(pointerButton);
    editToolBar->addWidget(linePointerButton);
    editToolBar->addSeparator();
    editToolBar->addAction(deleteAction);

}
void MainWindow::pointerClicked(int)
{
    scene->setMode(GraphScene::Mode(pointersGroup->checkedId()));

    qDebug() << "secene mode" << scene->getMode();
}

void MainWindow::backgroundsButtonClicked(QAbstractButton *button)
{

    QList<QAbstractButton *> buttons = backgroundButtonGroup->buttons();
    foreach (QAbstractButton *myButton, buttons) {
        if (myButton != button)
            button->setChecked(false);
    }
    QString text = button->text();
    if (text == tr("Ч-Б клетка")) {
        scene->setBackgroundBrush(QPixmap(":/icons/backgrounds/background2.png"));
    } else if (text == tr("Серая клетка")) {
        scene->setBackgroundBrush(QPixmap(":/icons/backgrounds/background3.png"));
    } else {
        scene->setBackgroundBrush(QPixmap(":/icons/backgrounds/background1.png"));
    }
    scene->update();
    view->update();
}
QWidget *MainWindow::createBackgroundWidget(const QString &text, const QString &image)
{
    QToolButton *button = new QToolButton;
    button->setText(text);
    button->setIcon(QIcon(image));
    button->setIconSize(QSize(50, 50));
    button->setCheckable(true);
    backgroundButtonGroup->addButton(button);
    QGridLayout *layout = new QGridLayout;
    layout->addWidget(button, 0, 0, Qt::AlignHCenter);
    layout->addWidget(new QLabel(text), 1, 0, Qt::AlignCenter);

    QWidget *widget = new QWidget;
    widget->setLayout(layout);

    return widget;
}

void MainWindow::deleteItem()
{
    foreach (QGraphicsItem *item, scene->selectedItems()) {
        if (item->type() == WireItem::Type) {
            scene->removeItem(item);
            WireItem *connection = qgraphicsitem_cast<WireItem *>(item);
            connection->startItem()->removeConnection(connection);
            connection->endItem()->removeConnection(connection);
            delete item;
        }
    }
    foreach (QGraphicsItem *item, scene->selectedItems()) {
        if (item->type() == PortItem::Type){
            qgraphicsitem_cast<PortItem *>(item)->clearConnections();
            qgraphicsitem_cast<DeviceItem *>(item->parentItem())->deletePort(qgraphicsitem_cast<PortItem *>(item));
            scene->removeItem(item);
            delete item;
        }
    }

    foreach (QGraphicsItem *item, scene->selectedItems()) {
        if (item->type() == DeviceItem::Type) {
            qgraphicsitem_cast<DeviceItem *>(item);
            if( !qgraphicsitem_cast<DeviceItem *>(item)->portsList().isEmpty()) {
                qgraphicsitem_cast<DeviceItem *>(item)->clearPorts();
            }
            scene->removeItem(item);
            scene->removeDevice(qgraphicsitem_cast<DeviceItem *>(item));
            delete item;
        }
    }
}
