#ifndef WIDGETWINDOW_H
#define WIDGETWINDOW_H

#include <QDebug>
#include <QMainWindow>
#include <QGraphicsView>
#include <QMenuBar>
#include <QHBoxLayout>
#include <QToolButton>
#include <QButtonGroup>
#include <QPushButton>
#include <QGraphicsBlurEffect>
#include <QGraphicsView>
#include <QToolBox>
#include <QToolBar>
#include <QMessageBox>
#include <QLabel>
#include "src/GuiManager/GraphScene.h"
//#include "src/ViewModels/WidgetItem.h"
//#include "src/ViewModels/WidgetLine.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();

    void createMenus();
    void createToolBox();
    void createToolBar();


private slots:
//    void devicesButtonClicked(int id);
//    void wiresButtonClicked(int id);
    void pointerClicked(int);
    void backgroundsButtonClicked(QAbstractButton *button);
    //for change scene mode to move item
//    void itemInserted(WidgetItem *item);

    void deleteItem();
private:
    GraphScene *scene;
    QGraphicsView *view;

    QToolBox *toolBox;
      QAction *deleteAction;
    QToolBar *editToolBar;

    QButtonGroup *devicesGroup;
    QButtonGroup *wiresGroup;
    //TODO think when add connector to devices
    QButtonGroup *pointersGroup;
    QButtonGroup *backgroundButtonGroup;
    QMenu *fileMenu;
    QMenu *editorMenu;

    QWidget *createBackgroundWidget(const QString &text,const QString &image);
//    QWidget *createDeviceWidget(const QString &text,  WidgetItem::WidgetItemType type);
//    QWidget *createCabelWidget(const QString &text,  WidgetLine::WidgetLineType type);
};

#endif // WIDGETWINDOW_H
