#include "Device.h"

Device::Device(int id, Device::DeviceType objectType)
{
    setId(id);
    setDeviceType(objectType);
}

Device::Device(const Device &t):
    m_id(t.m_id),
    m_deviceType(t.m_deviceType)
{

}

int Device::id() const
{
    return m_id;
}

void Device::setId(int id)
{
    m_id = id;
}

bool Device::getIsLinked() const
{
    return isLinked;
}

void Device::setIsLinked(bool value)
{
    isLinked = value;
}

void Device::setDeviceType(const DeviceType &objectType)
{
    m_deviceType = objectType;
}
