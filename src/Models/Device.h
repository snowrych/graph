#ifndef ROUTER_H
#define ROUTER_H

//#include <src/Models/BaseObject.h>
class QString;
class Device
{

public:
    enum DeviceType {Router,Switch,Diode};
    Device() = default;
    Device (int id,DeviceType deviceType);
    Device (const Device &t);

    /* getters and setters */
    int id() const;
    void setId(int id);

    DeviceType deviceType() const { return m_deviceType; }
    void setDeviceType(const DeviceType &deviceType);

    QString type() const;
    void setType(const QString &type);

    bool getIsLinked() const;
    void setIsLinked(bool value);
private:
    int m_id;
    DeviceType m_deviceType;
//    QString  _adress; //TODO  setters for parent class
    bool isLinked;

};

#endif // ROUTER_H
