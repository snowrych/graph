#include "Port.h"

Port::Port(int id, int deviceId, Port::PortType portType)
{
    setId(id);
    setDeviceId(deviceId);
    setPortType(portType);
}

Port::Port(const Port &pt) :
    m_id(pt.m_id),m_deviceId(pt.m_deviceId),
    m_portType(pt.portType())
{
}

void Port::setPortType(const PortType &portType)
{
    m_portType = portType;
}

int Port::id() const
{
    return m_id;
}

void Port::setId(int id)
{
    m_id = id;
}

int Port::deviceId() const
{
    return m_deviceId;
}

void Port::setDeviceId(int deviceId)
{
    m_deviceId = deviceId;
}
