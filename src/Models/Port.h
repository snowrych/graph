#ifndef WIRECONNECTOR_H
#define WIRECONNECTOR_H

//#include <src/Models/BaseObject.h>
class QString;
class Port
{
public:
    enum PortType {rj11,rj45,optical};
    Port() = default;

    Port (int id,int deviceId,PortType portType);
    Port (const Port &pt);

    /* getters and setters */
    PortType portType() const { return m_portType; }
    void setPortType(const PortType &portType);

    int id() const;
    void setId(int id);

    int deviceId() const;
    void setDeviceId(int deviceId);

private:
    int m_id;
    int m_deviceId;
    PortType m_portType;
    bool isLinked;
};

#endif // WIRECONNECTOR_H
