#include "Wire.h"

Wire::Wire(int id, int startPortId, int endPortId, Wire::WireType wireType)
{
    setId(id);
    setStartPortId(startPortId);
    setEndPortId(endPortId);
    setWireType(wireType);
}



Wire::Wire(const Wire &wr) :
     m_id(wr.m_id),
     m_startPortId(wr.m_startPortId),
     m_endPortId(wr.m_endPortId),
     m_wireType(wr.m_wireType)
{

}
void Wire::setWireType(const WireType &wireType)
{
    m_wireType = wireType;
}

int Wire::id() const
{
    return m_id;
}

void Wire::setId(int id)
{
    m_id = id;
}

int Wire::startPortId() const
{
    return m_startPortId;
}

void Wire::setStartPortId(int startPortId)
{
    m_startPortId = startPortId;
}

int Wire::endPortId() const
{
    return m_endPortId;
}

void Wire::setEndPortId(int endPortId)
{
    m_endPortId = endPortId;
}


