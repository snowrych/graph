#ifndef WIRE_H
#define WIRE_H

//#include <src/Models/BaseObject.h>
class QString;
class Wire
{
public:
    enum WireType {rj11,rj45,optical};
    Wire() = default;
    Wire (int id,int startPortId,int endPortId,WireType wireType);
    Wire (const Wire &wr);

    /* getters and setters */
    WireType wireType() const { return m_wireType; }
    void setWireType(const WireType &wireType);

    int id() const;
    void setId(int id);

    int startPortId() const;
    void setStartPortId(int startPortId);

    int endPortId() const;
    void setEndPortId(int endPortId);

    bool directed() const { return m_directed; }


private:
    int m_id;
    int m_startPortId;
    int m_endPortId;
    WireType m_wireType;
    bool m_directed = false;
};

#endif // WIRE_H
