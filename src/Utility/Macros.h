#ifndef MACROS_H
#define MACROS_H

#define SINGLETON(x) static x& instance() { static x s; return s; }

#endif // MACROS_H
