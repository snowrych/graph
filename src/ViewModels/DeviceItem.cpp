#include "DeviceItem.h"

DeviceItem::DeviceItem(Device *data, QGraphicsItem *parent) : QGraphicsItem(parent),
    m_deviceData(data)
{
    setFlags(ItemIsSelectable | ItemIsMovable);
    setFlag(QGraphicsItem::ItemClipsChildrenToShape,true);
    setAcceptHoverEvents(true);
    setZValue(0);
}
/* drawing methods */
QPainterPath DeviceItem::shape() const
{
    QPainterPath path;
    path.addRoundedRect(QRectF(0, 0, 100, 50), 10, 10);
    return path;

}

QRectF DeviceItem::boundingRect() const
{
    return QRectF(0, 0, 100, 50);
}

void DeviceItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->setBrush(Qt::darkYellow);
    painter->setPen(QPen(Qt::black, 0));
    painter->drawRoundedRect(QRectF(0, 0, 100, 50), 10, 10);
    if (isSelected()) {
        painter->setPen(QPen(Qt::black, 1, Qt::DashLine));
        painter->setBrush(Qt::NoBrush);
        QRectF frame =  QRectF(0, 0, 102, 52);
        frame.moveCenter(boundingRect().center());
        painter->drawRect(frame);
    }
}


/* mouse events */
void DeviceItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
}

void DeviceItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    setCursor(QCursor(Qt::SizeAllCursor));
    QGraphicsItem::mouseMoveEvent(event);
}

void DeviceItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
}

void DeviceItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::hoverMoveEvent(event);
}
QPointF DeviceItem::getCenterPos()
{
    return boundingRect().center();
}


void DeviceItem::addPort(PortItem *port)
{
    m_ports.append(port);
}

void DeviceItem::deletePort(PortItem *port)
{
    int index = m_ports.indexOf(port);

    if (index != -1)
        m_ports.removeAt(index);
}

void DeviceItem::clearPorts()
{
    foreach (PortItem *port, m_ports) {
        port->clearConnections();
        scene()->removeItem(port);
        delete port;
    }
}
