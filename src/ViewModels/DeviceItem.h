#ifndef OBJECTVM_H
#define OBJECTVM_H

#include <QString>
#include <QGraphicsItem>
#include <QPainterPath>
#include <QPainter>
#include <QCursor>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include "src/Models/Device.h"
#include "src/ViewModels/PortItem.h"
//#include "src/ViewModels/PortVm.h"
class PortItem;
class DeviceItem : public QGraphicsItem
{
public:
    //    ObjectVm();
    DeviceItem(Device* deviceData, QGraphicsItem* parent = nullptr); //TODO add other constructors

    enum { Type = Qt::UserRole + 2};
    virtual int type() const override { return Type; }

    Device* deviceData() const { return m_deviceData;}
    void setName(const QString& name) { m_name = name;}
    /* drawing methods */
    virtual QPainterPath shape() const override;
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

    void addPort(PortItem *port);
    void deletePort(PortItem *port);
    void clearPorts();
    QList<PortItem *> portsList() const{return m_ports;}

    QPointF getCenterPos();

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void hoverMoveEvent(QGraphicsSceneHoverEvent* event) override;

private:
    Device* m_deviceData;
//    Port* m_port;
    QList<PortItem*> m_ports;  //?? List<PortItem> ??
    QString m_name;
    //    QColor m_color;
};

#endif // OBJECTVM_H
