#include "PortItem.h"

PortItem::PortItem(Port *data, QGraphicsItem *parent) : QGraphicsItem(parent),
    m_portData(data)
{
    setFlags(ItemIsSelectable | ItemIsMovable);
    setAcceptHoverEvents(true);

}

/* drawing methods */
QPainterPath PortItem::shape() const
{
    QPainterPath path;
    path.addRect(QRectF(0, 0, 25, 12));
    return path;
}

QRectF PortItem::boundingRect() const
{
    return QRectF(0, 0, 20, 15);
}

void PortItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    painter->setBrush(Qt::darkGreen);
    painter->setPen(QPen(Qt::black, 0));
    painter->drawRect(QRectF(0, 0, 20, 15));
    if (isSelected())
    {
        painter->setPen(QPen(Qt::black, 1, Qt::DashLine));
        painter->setBrush(Qt::NoBrush);
        QRectF frame =  QRectF(0, 0, 22, 16);
        frame.moveCenter(boundingRect().center());
        painter->drawRect(frame);
    }
}

void PortItem::addConnection(WireItem *connection)
{
    m_wires.append(connection);
}

void PortItem::removeConnection(WireItem *w)
{
    int index = m_wires.indexOf(w);
    if(index != -1 ) {
        m_wires.removeAt(index);
    }
}

void PortItem::clearConnections()
{
    foreach (WireItem *connnection, m_wires) {
        connnection->startItem()->removeConnection(connnection);
        connnection->endItem()->removeConnection(connnection);
        scene()->removeItem(connnection);
        delete connnection;
    }
}


QPointF PortItem::getOnScenePos()
{
    return mapToScene(boundingRect().center());
}


/* mouse events */
void PortItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
}

void PortItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    setCursor(QCursor(Qt::SizeAllCursor));
    QGraphicsItem::mouseMoveEvent(event);

    //    qDebug() << "parentX Y " << x() << y();
}

void PortItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    setCursor(QCursor(Qt::ArrowCursor));
    if(this->x() > 79  ) this->setX(79); //TODO переделать moveArea
    if(this->x() < 0 ) this->setX(0);
    if(this->y() > 32  ) this->setY(32);
    if(this->y() < 0 ) this->setY(0);

    QGraphicsItem::mouseReleaseEvent(event);
}

void PortItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::hoverMoveEvent(event);
}
