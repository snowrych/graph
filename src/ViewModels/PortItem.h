#ifndef PORTITEM_H
#define PORTITEM_H

#include <QString>
#include <QGraphicsItem>
#include <QPainterPath>
#include <QPainter>
#include <QCursor>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include "src/Models/Port.h"
#include "src/Models/Wire.h"
#include "src/ViewModels/WireItem.h"
class WireItem;
class PortItem : public QGraphicsItem
{
public:
    PortItem(Port* portData, QGraphicsItem* parent = nullptr); //TODO add other constructors

    enum { Type = Qt::UserRole + 15};
    virtual int type() const override { return Type; }
    Port* portData() const { return m_portData;}

    /* drawing methods */
    virtual QPainterPath shape() const override;
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

    void addConnection(WireItem* connection);
    QList<WireItem*> getConnectionList() const { return m_wires; }
    void removeConnection(WireItem* w);
    void clearConnections();

    QPointF getOnScenePos();

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void hoverMoveEvent(QGraphicsSceneHoverEvent* event) override;

private:
    Port* m_portData;
    QList<WireItem*> m_wires;//?? list<WireItem*> ??
};

#endif // PORTITEM_H
