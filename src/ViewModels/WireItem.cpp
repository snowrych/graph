#include "WireItem.h"
#include <QtMath>


WireItem::WireItem(PortItem *start, PortItem *end, Wire *wireData, QGraphicsItem *parent) :
    QGraphicsLineItem(parent)
{
    setFlag(GraphicsItemFlag::ItemIsSelectable, true);
    setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    m_startNode = start;
    m_endNode = end;
    m_wireData = wireData;
    setAcceptHoverEvents(true);
    setZValue(1);

}

//WireItem::WireItem(Wire *wireData, QGraphicsItem *parent) :  QGraphicsLineItem(parent),
//    m_wireData(wireData)
//{
//    setFlag(GraphicsItemFlag::ItemIsSelectable, true);
//    setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
//    setZValue(-100);
//}

// drawing methods
//QRectF WireItem::boundingRect() const
//{
//    qreal lineWidth = qSqrt(pow(line().x2() - line().x1(),2.0) + pow(line().y2() - line().y1(),2.0)) ;
//    return QRectF(0, 0, lineWidth,this->pen().width());

//}
QRectF WireItem::boundingRect() const
{
    qreal extra = (pen().width() + 20) / 2.0;
    QRectF boundingRect = QRectF(line().p1(), QSizeF(line().p2().x() - line().p1().x(),
                                                     line().p2().y() - line().p1().y()))
            .normalized()
            .adjusted(-extra, -extra, extra, extra);
    if (!boundingRect.contains(m_arcCenter))
        return boundingRect.united(QRectF(m_arcCenter, QSize(1, 1)));
    return boundingRect;
}

QPainterPath WireItem::shape() const
{
        QPainterPath path = QGraphicsLineItem::shape();
        path.addPolygon(m_arrowHead);

        QPointF vec = line().p2() - line().p1();
        vec = vec*(3/qSqrt(QPointF::dotProduct(vec, vec)));
        QPointF orthogonal(vec.y(), -vec.x());
        QPainterPath result(line().p1()-vec+orthogonal);
            result.lineTo(line().p1()-vec-orthogonal);
            result.lineTo(line().p2()+vec-orthogonal);
            result.lineTo(line().p2()+vec+orthogonal);
            result.closeSubpath();
        path.addPath(result);
        return path;
}
void WireItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)
    Q_UNUSED(option)

    if (m_startNode->collidesWithItem(m_endNode))
        return;

    QPen p = pen();
    p.setColor(Qt::black);
    p.setWidth(2);
    painter->setBrush(Qt::blue);
    painter->setPen(p);

    switch (lineType()) {
    case Line:
        setLine(QLineF(m_startNode->getOnScenePos(), m_endNode->getOnScenePos()));
        painter->drawLine(line());
        break;
    case LineWithArrow: {
        setLine(QLineF(m_startNode->getOnScenePos(), m_endNode->getOnScenePos()));
        m_path = QGraphicsLineItem::shape();
        qreal angle = line().angle();
        QPointF arrowHead((m_startNode->boundingRect().width() /2) * qCos((-angle + 180) * M_PI / 180) + m_endNode->getOnScenePos().x(),
                          (m_startNode->boundingRect().width() /2) * qSin((-angle + 180) * M_PI / 180) + m_endNode->getOnScenePos().y());
        painter->drawPoint(arrowHead);
        QLineF endRadiusLine(line().p2(), arrowHead);
        double angle2 = std::atan2(-endRadiusLine.dy(), endRadiusLine.dx());

        QPointF arrowP1 = arrowHead + QPointF(qSin(angle2 + M_PI / 3) * 10,
                                              qCos(angle2 + M_PI / 3) * 10);
        QPointF arrowP2 = arrowHead + QPointF(qSin(angle2 + M_PI - M_PI / 3) * 10,
                                              qCos(angle2 + M_PI - M_PI / 3) * 10);
        m_arrowHead.clear();
        m_arrowHead << arrowHead << arrowP1 << arrowP2;
        painter->drawLine(line());
        painter->drawPolygon(m_arrowHead);
        break;
    }
    case Arc: {
        setLine(QLineF(m_startNode->getOnScenePos(), m_endNode->getOnScenePos()));
        QLineF nv(QLineF(line().center(), m_endNode->getOnScenePos()).normalVector());
        m_arcCenter = nv.pointAt(0.2);
        QPainterPath path(line().p1());
        path.quadTo(m_arcCenter, m_endNode->getOnScenePos());
        QPainterPathStroker stroker;
        QPainterPath outline = stroker.createStroke(path);
        m_path = outline;
        painter->drawPath(m_path);
        break;
    }
    case ArcWithArrow: {
        setLine(QLineF(m_startNode->getOnScenePos(), m_endNode->getOnScenePos()));
        QLineF nv(QLineF(line().center(), m_endNode->getOnScenePos()).normalVector());
        m_arcCenter = nv.pointAt(0.2);
        QPainterPath path(line().p1());
        path.quadTo(m_arcCenter, m_endNode->getOnScenePos());
        QPainterPathStroker stroker;
        QPainterPath outline = stroker.createStroke(path);
        m_path = outline;

        qreal angle = path.angleAtPercent(0.8);
        QPointF arrowHead((m_endNode->boundingRect().width() /2) * qCos((-angle + 180) * M_PI / 180) + m_endNode->getOnScenePos().x(),
                          (m_endNode->boundingRect().width() /2) * qSin((-angle + 180) * M_PI / 180) + m_endNode->getOnScenePos().y());
        QLineF temp(path.currentPosition(), arrowHead);
        double angle2 = std::atan2(-temp.dy(), temp.dx());

        QPointF arrowP1 = arrowHead + QPointF(qSin(angle2 + M_PI / 3) * 10,
                                              qCos(angle2 + M_PI / 3) * 10);
        QPointF arrowP2 = arrowHead + QPointF(qSin(angle2 + M_PI - M_PI / 3) * 10,
                                              qCos(angle2 + M_PI - M_PI / 3) * 10);
        m_arrowHead.clear();
        m_arrowHead << arrowHead << arrowP1 << arrowP2;
        painter->drawPath(m_path);
        painter->drawPolygon(m_arrowHead);
        break;
    }
    }

    // line frame
    if(isSelected()) {
        painter->setPen(QPen(Qt::black, 1, Qt::DashLine));
        painter->setBrush(Qt::NoBrush);
        painter->drawPath(shape());
    }
}

void WireItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);

}

void WireItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);
}

void WireItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
}

void WireItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::hoverMoveEvent(event);
}

WireItem::LineType WireItem::lineType() const
{
    //    if(m_wireData->directed()) {
    //        return LineType::LineWithArrow;
    //    }

    //TODO не работает условие (доделать)
    //    if(m_startNode->isSameConnection(m_wireData) && m_endNode->isSameConnection(m_wireData)) {
    //        return LineType::Arc;
    //    } else {
    //        return LineType::Line;
    //    }
    return LineType::LineWithArrow;
}

