#ifndef WIREVM_H
#define WIREVM_H

#include <QGraphicsLineItem>
#include "src/Models/Wire.h"
#include "src/ViewModels/PortItem.h"
#include "src/GuiManager/GraphScene.h"

class GraphScene;
class PortItem;
class WireItem : public QGraphicsLineItem
{
public:
    enum { Type = UserType + 10 };
    enum LineType {Line,LineWithArrow,Arc,ArcWithArrow};

    explicit WireItem(PortItem* start, PortItem* end,Wire* wireData, QGraphicsItem* parent = nullptr);
    //      WireItem(Wire* wireData, QGraphicsItem* parent = nullptr); //TODO add other constructors

    Wire* wireData() const { return m_wireData;}

//    LineType lineType() const { return m_lineType;}
//    void setLineType(const LineType &lineType);

    PortItem* startItem() const { return m_startNode;}
    PortItem* endItem()  const { return m_endNode;}

    // QGraphicsItem interface
    virtual int type() const override { return Type; }

    QRectF boundingRect() const override;
    QPainterPath shape() const override;

protected:
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
    virtual void hoverMoveEvent(QGraphicsSceneHoverEvent* event) override;


private:
    LineType lineType() const;
    Wire* m_wireData;
    PortItem* m_startNode;
    PortItem* m_endNode;
    QPainterPath m_path;
    QPainterPath m_path2;

    QPolygonF m_arrowHead;

    QPointF m_arcCenter;

};

#endif // WIREVM_H
