#include <QApplication>
#include "src/GuiManager/MainWindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    w.setWindowTitle ("Test graph");
    w.setWindowIcon (QIcon(":/icons/main.png"));
    w.resize (720,480);
    w.show();

    return a.exec();
}
